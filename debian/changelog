tinyirc (1:1.1.dfsg.1-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 26 Oct 2022 13:41:17 +0100

tinyirc (1:1.1.dfsg.1-4) unstable; urgency=medium

  * QA upload.

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make and stop
    overriding CC. (Closes: #951015)

  [ Jari Aalto ]
  * debian/control
    - (Standards-Version): Update to 3.9.8.
    - (Vcs-*): Update to anonscm.debian.org.
  * debian/copyright
    - Update URLs.
    - Update Debian maintainers.
    - Update upstream email addresses and comment field.
  * debian/source.lintian-overrides
    - ACK for need of Debian numbering change after upstream release.
  * debian/patches
    - (20): Modify. Add -h, --help, --dumb options. Activate ncurses
      properly. Respect NICK command line argument (Closes: #712887).
  * debian/rules
    - Compile with --std=c99 and ncurses.
  * debian/tinyirc.1.pod
    - (DESCRIPTION): Mention no editing capabilities (Closes: #712888).
    - (OPTIONS): Add options -h, --help and --dumb.
    - (AUTHORS): Replace template name Foo with Nathan Laredo.

  [ Reiner Herrmann ]
  * Make the build reproducible: extract build date from last changelog entry
    in debian/pod2man.mk (Closes: #782223)

  [ Baptiste BEAUPLAT ]
  * Update links:
    + Set Vcs-* to salsa project in d/control
    + Switch to https in d/README.Debian
  * d/control:
    + Set Maintainer to Debian QA Group
    + Bump Standard-Version to 4.5.0
    + Bump debhelper-compat to 13 (d/compat removed)
    + Add Rules-Requires-Root: no
  * d/rules: Remove explicit linker flag --as-needed as this is now the default
  * Add version to dummy d/watch
  * Add salsa pipeline CI
  * Move lintian-overrides to d/source/
  * Fix typo in lintian-overrides
  * Use recommended branch name from DEP-14

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Fri, 28 Aug 2020 16:57:19 +0200

tinyirc (1:1.1.dfsg.1-3) unstable; urgency=low

  * New maintainer (Closes: #660510).
    - Upgrade to packaging format "3.0 (quilt)".
  * debian/clean
    - New file.
  * debian/compat
    - Update from 4 to 9.
  * debian/control
    - (Build-Depends): Update to debhelper 9.
    - (Description): remove article (Lintian).
    - (Depends): Add ${shlibs:Depends}.
    - (Standards-Version): Update from 3.6.2 to 3.9.3.
    - (Vcs-*): Add new fields.
  * debian/copyright
    - Update to Format 1.0.
  * debian/install
    - New file.
  * debian/menu
    - (section): Update to Applications/Network/Communication.
  * debian/pod2man.mk
    - New file.
  * debian/patches
    - (20): New; argument handling. Allow any port number (Closes: #625862).
  * debian/rules
    - Convert to dh(1).
    - Use all hardened build flags.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags
  * debian/tinyirc.pod
    - Convert raw *.1 to maintainable *.pod format.
    - Clarify arguments (Closes: #499692).
  * debian/tinyirc.*
    - Simplify files by dropping tinyirc.* prefix.
      Like tinyirc.manpages => manpages.
  * debian/watch
    - New file.
  * Makfile, tinyirc.c
    - Restore original sources.

 -- Jari Aalto <jari.aalto@cante.net>  Wed, 22 Aug 2012 16:26:30 +0300

tinyirc (1:1.1.dfsg.1-2) unstable; urgency=low

  * QA upload
  * set maintainer to Debian QA Group
  * split monolithic .diff into patches to make them more readable
  * record new upstream address and add homepage field (Closes: 441441)
  * use dpkg-buildflags (with hardening=+all)
  * fix NULL dereferences (Closes: 452029)
  * add build-arch and build-indep targets
  * add missing prerequisite of install target
  * update to new menu policy

 -- Bernhard R. Link <brlink@debian.org>  Mon, 02 Jul 2012 16:52:42 +0200

tinyirc (1:1.1.dfsg.1-1) unstable; urgency=low

  * Repack source without RFC 1459.
  * Bump Standards-Version to 3.6.2.

 -- Decklin Foster <decklin@red-bean.com>  Sat,  6 Aug 2005 13:10:02 -0400

tinyirc (1:1.1-11) unstable; urgency=low

  * Fix typo in package description (Closes: #277255)

 -- Decklin Foster <decklin@red-bean.com>  Mon, 25 Oct 2004 01:55:38 -0400

tinyirc (1:1.1-10) unstable; urgency=low

  * Updated URL reference to RFC 1459 (Closes: #262656)
  * Bumped Standards-Version to 3.6.1.

 -- Decklin Foster <decklin@red-bean.com>  Sat,  2 Oct 2004 07:11:00 -0400

tinyirc (1:1.1-9) unstable; urgency=low

  * Provide irc alternative (Closes: #183015, #204664)
  * Add isatty(3) check in case of stdin (Closes: #176213)
  * Update to debhelper v4 and policy 3.6.0

 -- Decklin Foster <decklin@red-bean.com>  Tue, 19 Aug 2003 10:17:50 -0400

tinyirc (1:1.1-8) unstable; urgency=low

  * debian/changelog: removed local Emacs settings.
  * Changed man page section from 1x to 1 (whoops).

 -- Decklin Foster <decklin@red-bean.com>  Fri,  1 Feb 2002 14:49:36 -0500

tinyirc (1:1.1-7) unstable; urgency=low

  * Updated priority from from extra to optional to match override
    file.

 -- Decklin Foster <decklin@red-bean.com>  Sat,  2 Jun 2001 16:06:10 -0400

tinyirc (1:1.1-6) unstable; urgency=low

  * Added build-dep on libncurses5-dev, and several missing #includes
    - Should now build on arm (Closes: #99030)
  * Added a few small patches:
    - Print channel name correctly for /invites
    - Fix definition of bp for tgetent call
    - Clean up several warnings
  * Use debhelpher v3 and policy 3.5.4
    - added DEB_BUILD_OPTIONS support
  * Fixed URL typo in debian/copyright

 -- Decklin Foster <decklin@red-bean.com>  Fri,  1 Jun 2001 16:54:21 -0400

tinyirc (1:1.1-5) unstable; urgency=low

  * New maintainer (Closes: #81891)
  * Recompile against libncurses5 (Closes: #63369)
  * Updated debian/copyright
  * Wrote a man page (Closes: #68823)
  * Wrote patch to prevent segfault on /part (Closes: #70671)
  * Wrote patch to suppress extra fields in GECOS (Closes: #66443)
  * Updated debhelper rules to v2
  * debian/control: added Build-Depends, bumped Standards-Version to
    3.2.1.0

 -- Decklin Foster <decklin@red-bean.com>  Fri, 12 Jan 2001 15:59:33 -0500

tinyirc (1:1.1-4) unstable; urgency=low

  * Rebuilt to make use of the new ncurses stuffs.

 -- Jay Kominek <jkominek@debian.org>  Fri, 30 Oct 1998 22:45:41 -0700

tinyirc (1:1.1-3) unstable; urgency=low

  * Converted to use debhelper.
  * Removed server-numerics from the documentation, as it is not
    particularly useful or interesting.
  * Corrected a bug which prevented TinyIRC from function with
    servers that used the nospoof patch.

 -- Jay Kominek <jkominek@debian.org>  Sun,  8 Mar 1998 22:21:53 -0500

tinyirc (1:1.1-2) unstable; urgency=low

  * Fixed portability problem in the Makefile.

 -- Jay Kominek <jkominek@xtn.net>  Sun,  2 Nov 1997 12:16:24 -0500

tinyirc (1:1.1-1) unstable; urgency=low

  * New upstream release.

 -- Jay Kominek <jkominek@xtn.net>  Thu, 16 Oct 1997 17:08:41 -0400

tinyirc (pre1.0-1) unstable; urgency=low

  * Initial Release.

 -- Jay Kominek <jkominek@xtn.net>  Sun, 28 Sep 1997 10:22:08 -0400
